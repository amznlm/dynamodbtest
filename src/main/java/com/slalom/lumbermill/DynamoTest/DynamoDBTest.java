package com.slalom.lumbermill.DynamoTest;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.google.common.collect.Sets;

import java.util.Date;

public class DynamoDBTest
{
    public static void main(String[] args)
    {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient()
                .withRegion(Regions.US_EAST_1);

        DynamoDBMapper mapper = new DynamoDBMapper(client);

        UserModel user = createUserModel();

        insertSingleUser(mapper, user);
    }

    private static UserModel createUserModel()
    {
        UserModel user = new UserModel();
        user.setUsername("rollinb");
        user.setAge(29);
        user.setGender("male");
        user.setLocation("Seattle, WA");
        user.setGenres(Sets.newHashSet("RTS", "FPS"));
        return user;
    }

    private static void insertSingleUser(DynamoDBMapper mapper, UserModel user)
    {
        System.out.println("Saving user: " + user.getUsername());
        mapper.save(user);
        System.out.println("User saved: " + user.getUsername());
    }
}