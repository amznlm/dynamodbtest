package com.slalom.lumbermill.DynamoTest;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.Set;

@DynamoDBTable(tableName="users")
public class UserModel
{
    private String username;
    private String gender;
    private Integer age;
    private String location;
    private Set<String> genres;

    @DynamoDBHashKey(attributeName="username")
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    @DynamoDBAttribute(attributeName="gender")
    public String getGender()
    {
        return gender;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    @DynamoDBAttribute(attributeName="age")
    public Integer getAge()
    {
        return age;
    }

    public void setAge(Integer age)
    {
        this.age = age;
    }

    @DynamoDBAttribute(attributeName="location")
    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    @DynamoDBAttribute(attributeName="genres")
    public Set<String> getGenres()
    {
        return genres;
    }

    public void setGenres(Set<String> genres)
    {
        this.genres = genres;
    }
}
